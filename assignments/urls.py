from django.urls import path

from assignments.views import AssignmentPageView, CreateAssignmentView, CreateTagView, EditAssignmentView

urlpatterns = [
    path('<int:pk>', AssignmentPageView.as_view(), name='assignment'),
    path('create_assignment', CreateAssignmentView.as_view(), name='create_assignment'),
    path('edit_assignment/<int:pk>', EditAssignmentView.as_view(), name='edit_assignment'),
    path('create_tag', CreateTagView.as_view(), name='create_tag')
]

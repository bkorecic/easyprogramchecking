from django.contrib import admin
from assignments.models import Assignment, Tag, Language

admin.site.register(Assignment)
admin.site.register(Language)
admin.site.register(Tag)

import os
from django.db import models
from django.utils import timezone
from django.urls import reverse
from courses.models import Course
from django.core.files import File
from django.db.models.signals import post_save
from django.dispatch import receiver

from my_lib.files_wrapper import file_to_file, change_path_extension, get_file_name, check_if_file_is_valid


class Language(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=40, unique=True)

    def __str__(self):
        return "#" + self.name


class Assignment(models.Model):
    title = models.CharField(max_length=100)
    created_at = models.DateTimeField(editable=False, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deadline = models.DateTimeField(blank=True, null=True)
    published = models.BooleanField(default=False)
    tags = models.ManyToManyField(Tag, blank=True)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, null=False)

    statement = models.FileField(upload_to='statements', blank=True, null=True)

    # Matipedia: I'm not sure how the period is stated in chars
    period = models.CharField(max_length=20, default="", blank=True, null=True)

    # source: https://docs.djangoproject.com/en/dev/ref/models/fields/#foreignkey
    original = models.ForeignKey('self', default=None, on_delete=models.PROTECT, blank=True, null=True)

    # The source with which the statement file was created (a .zip or another compression format)
    source = models.FileField(upload_to='source', blank=True, null=True)

    tests = models.FileField(upload_to='test_files', blank=True, null=True)

    allowed_languages = models.ManyToManyField(Language)

    def save(self, *args, **kwargs):
        """
        On save, update timestamps
        """
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()

        return models.Model.save(self, *args, **kwargs)

    def __str__(self):
        return self.course.code + ": " + self.title

    def get_absolute_url(self):
        return reverse('assignment', kwargs={'pk': self.pk})

    def get_source_name(self):
        return self.source.name.split("/")[-1]

    def get_statement_name(self):
        return self.statement.name.split("/")[-1]

    def get_tests_name(self):
        return self.tests.name.split("/")[-1]


@receiver(post_save, sender=Assignment)
def create_assignment(sender, instance, created, **kwargs):
    """
    Changes the file to a json on creation

    :param sender: The model class. (Assignment)
    :param instance: The actual instance being saved.
    :param created: Boolean; True if a new record was created.
    """

    if created and instance.tests:
        # Generates paths for the new json file
        test_url = instance.tests.url[1:]
        json_tests_url = change_path_extension(test_url, 'json')

        if not check_if_file_is_valid(test_url):
            instance.tests = None
            instance.save()
            os.remove(test_url)
            return

        # Passes the information from the original file to the json file
        file_to_file(test_url, json_tests_url)

        # Saves the new json file as the tests file in the instance
        f = open(json_tests_url)
        instance.tests.save(get_file_name(json_tests_url), File(f))
        f.close()

        # Deletes the old file
        os.remove(test_url)

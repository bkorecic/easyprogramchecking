import os
from django.utils import timezone
from django.http import HttpResponseRedirect, Http404
from django.urls import reverse
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView
from django.shortcuts import get_object_or_404
from assignments.models import Assignment, Tag
from assignments.forms import CreateAssignmentForm, EditAssignmentForm, CreateTagForm
from users.models import Account
from feedback.models import Feedback, PublicTestFeedback, SemiPrivateTestFeedback, PrivateTestFeedback
from feedback.scripts import SaveScriptProcess
from django.shortcuts import render
from django.core.files import File
from django.core.files.storage import FileSystemStorage
from django.contrib import messages
from my_lib.files_wrapper import check_if_file_is_valid, file_to_file, change_path_extension, get_file_name

from scriptserver.comunication.client import Client

ComunicationClient = Client()


class AssignmentPageView(TemplateView):
    template_name = "assignments/assignment_page.html"
    feedback_template_name = "feedback/immediate_feedback_page.html"
    feedback_error_template_name = "feedback/immediate_feedback_error_page.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        assignment = get_object_or_404(Assignment, id=self.kwargs['pk'])

        context['title'] = assignment.title

        context['teachers'] = Account.objects.filter(teaching_courses__id=assignment.course.id)
        context['assistants'] = Account.objects.filter(assistant_courses__id=assignment.course.id)

        context['isTeacher'] = context['teachers'].filter(id=self.request.user.id).exists()
        context['isAssistant'] = context['assistants'].filter(id=self.request.user.id).exists()

        if not assignment.published and not context['isTeacher'] and not context['isAssistant']:
            raise Http404

        context['input_examples'] = []
        context['output_examples'] = []

        context['has_tests'] = bool(assignment.tests)

        context['assignment'] = assignment

        context['tags'] = assignment.tags.all()

        assignment_course = assignment.course
        context['course'] = assignment_course
        context['course_url'] = assignment_course.get_absolute_url()
        context['langs'] = assignment.allowed_languages.all()
        return context

    def post(self, request, *args, **kwargs):
        # Get assignment and post data
        assignment = get_object_or_404(Assignment, id=self.kwargs['pk'])

        if not bool(assignment.tests):
            return self.handle_failed_single_response(request,
                                                      "The Assignment has no tests, talk to the course's teacher or assistants to fix this issue.",
                                                      **kwargs)

        data = request.POST.copy()

        # Get language and file from data
        lang = data.get('language')
        file = request.FILES.get('sample_code')

        # If the user didn´t uploaded a file it sends the user back to the same page
        if file == None:
            return render(request, self.template_name, self.get_context_data(**kwargs))

        # Save the file in the media folder
        fs = FileSystemStorage()
        filename = fs.save(file.name, file)

        # get the path from the media folder
        script_path = os.getcwd().replace("\\", "/") + "/media/" + filename
        test_path = assignment.tests.path.replace("\\", "/")

        if data["submission_type"] == "single":

            # Send and receive data for testing
            response = ComunicationClient.send_submission(script_path, test_path, lang)

            # Delete the source code file from the media folder
            os.remove('media/' + filename)
            if lang == 'C++11' or lang == 'Java 11':
                if lang == 'C++11':
                    out_name = os.path.splitext(filename)[0] + '.out'
                elif lang == 'Java 11':
                    out_name = os.path.splitext(filename)[0] + '.class'
                os.remove('media/' + out_name)

            if response[0] == "success":
                return self.handle_successful_single_response(request, assignment, response[1], **kwargs)
            else:
                return self.handle_failed_single_response(request, response[1], **kwargs)
        elif data["submission_type"] == "multiple":

            SaveScriptProcess(request.user.account, assignment, lang, ComunicationClient, script_path,
                              test_path).start()

            this_context = self.get_context_data(**kwargs)

            return render(request, self.template_name, this_context)
        else:

            # Delete the source code file from the media folder
            os.remove('media/' + filename)
            if lang == 'C++11' or lang == 'Java 11':
                if lang == 'C++11':
                    out_name = os.path.splitext(filename)[0] + '.out'
                elif lang == 'Java 11':
                    out_name = os.path.splitext(filename)[0] + '.class'
                os.remove('media/' + out_name)

            raise Exception("Unidentified submission")

    def handle_successful_single_response(self, request, assignment, tests_arr, **kwargs):
        # Save tests_arr as a feedback object and a group of simple_test_feedback objects
        if request.user.__str__() != "AnonymousUser":
            # Create the feedback object and save it in the database
            feedback_user = request.user.account
            feedback_assignment = assignment
            feedback_date = timezone.now()
            feedback = Feedback(user=feedback_user, assignment=feedback_assignment, date=feedback_date)
            feedback.save()

            # Save the tests as single_test_feedback object of diferent types depending on the test type
            for test in tests_arr:
                single_test_feedback = None

                # Case the test is public
                if test[4] == "public":
                    single_test_feedback = PublicTestFeedback(passed=bool(test[0]), input=test[1],
                                                              expected_output=test[2], actual_output=test[6],
                                                              comment=test[3], error=test[5],
                                                              feedback=feedback)
                # Case the test is semi private
                elif test[4] == "semi_private":
                    single_test_feedback = SemiPrivateTestFeedback(passed=bool(test[0]), input=test[1],
                                                                   expected_output=test[2], actual_output=test[6],
                                                                   comment=test[3],
                                                                   error=test[5],
                                                                   feedback=feedback)
                # Case the test is private
                else:
                    single_test_feedback = PrivateTestFeedback(passed=bool(test[0]), input=test[1],
                                                               expected_output=test[2], actual_output=test[6],
                                                               comment=test[3], error=test[5],
                                                               feedback=feedback)

                single_test_feedback.save()
        else:
            # Create the feedback object and save it in the database
            feedback_user = None
            feedback_assignment = assignment
            feedback_date = timezone.now()
            feedback = Feedback(user=feedback_user, assignment=feedback_assignment, date=feedback_date)
            feedback.save()

        this_context = self.get_context_data(**kwargs)

        passed_tests_dict_arr = []
        failed_tests_dict_arr = []

        i = 1
        for test in tests_arr:
            elem = dict()
            elem["num"] = i
            elem["input"] = test[1]
            elem["expected_output"] = test[2]
            elem["actual_output"] = test[6]
            elem["comment"] = test[3]
            elem["type"] = test[4]
            elem["error_code"] = test[5]
            if int(test[5]) == 0:
                elem["error"] = False
            else:
                elem["error"] = True
                if int(test[5]) == 1:
                    elem["error_detail"] = "Runtime or Compilation Error"
                elif int(test[5]) == 2:
                    elem["error_detail"] = "Timeout Error"
                else:
                    elem["error_detail"] = "Unknown Error"
            if not (elem["type"] == "private" and (not (this_context["isTeacher"] or this_context["isAssistant"]))):
                if bool(test[0]):
                    passed_tests_dict_arr.append(elem)

                else:
                    failed_tests_dict_arr.append(elem)

                i += 1

        if tests_arr == []:
            return render(request, self.template_name, self.get_context_data(**kwargs))
        else:
            this_context['passed_tests'] = passed_tests_dict_arr
            this_context['failed_tests'] = failed_tests_dict_arr
            return render(request, self.feedback_template_name, this_context)

    def handle_failed_single_response(self, request, error, **kwargs):
        this_context = self.get_context_data(**kwargs)
        this_context['error'] = error
        return render(request, self.feedback_error_template_name, this_context)


class CreateAssignmentView(CreateView):
    template_name = "assignments/assignment_form.html"
    model = Assignment
    form_class = CreateAssignmentForm

    obj = ""

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        teaching_courses = self.request.user.account.teaching_courses.all()
        assistant_courses = self.request.user.account.assistant_courses.all()
        context['available_courses'] = (teaching_courses | assistant_courses).distinct()
        return context

    def form_valid(self, form):
        self.obj = form.save(commit=False)

        teaching_courses = self.request.user.account.teaching_courses.all()
        assistant_courses = self.request.user.account.assistant_courses.all()

        available_courses = (teaching_courses | assistant_courses).distinct()

        # Check if the course is in the available courses
        if self.obj.course not in available_courses:
            return HttpResponseRedirect(self.request.path_info)

        added_tests = bool(self.obj.tests)

        # save object
        self.obj.save()

        # add tags
        for tag in form.cleaned_data['tags']:
            self.obj.tags.add(tag)

        # add langs
        for lang in form.cleaned_data['allowed_languages']:
            self.obj.allowed_languages.add(lang)

        # If tests were added but were deleted by the system for being invalid
        if not bool(self.obj.tests) and added_tests:
            # Delete Assignment
            Assignment.objects.filter(id=self.obj.id).delete()
            form.add_error('tests', 'The test cases file is not valid.')
            return self.form_invalid(form)

        return self.get_success_url()

    def get_form_kwargs(self, **kwargs):
        form_kwargs = super(CreateAssignmentView, self).get_form_kwargs()
        form_kwargs["user"] = self.request.user
        return form_kwargs

    def get_success_url(self):
        return HttpResponseRedirect(reverse('assignment', kwargs={'pk': self.obj.id}))


class EditAssignmentView(UpdateView):
    model = Assignment
    template_name = 'assignments/assignment_edit.html'
    form_class = EditAssignmentForm

    def form_valid(self, form):
        teaching_courses = self.request.user.account.teaching_courses.all()
        assistant_courses = self.request.user.account.assistant_courses.all()
        available_courses = (teaching_courses | assistant_courses).distinct()
        if self.object.course not in available_courses:
            return HttpResponseRedirect(self.request.path_info) # TODO: Replace this with UserPassesTestMixin

        # Check if test_file is valid
        test_url = self.object.tests.url[1:]
        print(test_url)
        if not check_if_file_is_valid(test_url):
            self.object.save()
            form.add_error('tests', 'The test cases file is not valid.')
            return self.form_invalid(form)
        else:
            json_tests_url = change_path_extension(test_url, 'json')
            file_to_file(test_url, json_tests_url)

            # Saves the new json file as the tests file in the instance
            f = open(json_tests_url)
            self.object.tests.save(get_file_name(json_tests_url), File(f))
            f.close()

            # Deletes the old file
            os.remove(test_url)
        return super().form_valid(form)

    """
    def form_valid(self, form):
        self.obj = get_object_or_404(Assignment, id=self.kwargs['pk'])

        if self.obj.tests:
            test_url = self.obj.tests.url[1:]
            previous_tests = self.obj.tests
        else:
            test_url = ""
            previous_tests = None

        form.save_all(self.obj.id)

        self.obj = get_object_or_404(Assignment, id=self.kwargs['pk'])

        teaching_courses = self.request.user.account.teaching_courses.all()
        assistant_courses = self.request.user.account.assistant_courses.all()

        available_courses = (teaching_courses | assistant_courses).distinct()

        # Check if test_file is valid
        test_url = self.obj.tests.url[1:]
        if not check_if_file_is_valid(test_url):
            self.obj.tests = previous_tests
            self.obj.save()
            form.add_error('tests', 'The test cases file is not valid.')
            return self.form_invalid(form)
        else:
            json_tests_url = change_path_extension(test_url, 'json')
            file_to_file(test_url, json_tests_url)

            # Saves the new json file as the tests file in the instance
            f = open(json_tests_url)
            self.obj.tests.save(get_file_name(json_tests_url), File(f))
            f.close()

            # Deletes the old file
            os.remove(test_url)

        # Check if the course is in the available courses
        if self.obj.course not in available_courses:
            return HttpResponseRedirect(self.request.path_info)

        return self.get_success_url()
    """


class CreateTagView(CreateView):
    template_name = "tags/tag_form.html"
    model = Tag
    form_class = CreateTagForm

    def get_success_url(self):
        messages.success(self.request, 'Tag created!')
        return reverse('create_tag')

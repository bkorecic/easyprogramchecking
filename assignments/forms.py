from django import forms
from assignments.models import Assignment, Tag, Language
from tempus_dominus.widgets import DateTimePicker
from django.db.models import Q


class CreateAssignmentForm(forms.ModelForm):
    deadline = forms.DateTimeField(
        required=False,
        widget=DateTimePicker(
            options={
                'useCurrent': True,
                'collapse': False,
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': True,
            },
        ),
    )

    course = forms.ModelChoiceField(queryset=None)

    class Meta:
        model = Assignment
        fields = ['title', 'allowed_languages', 'tags', 'statement', 'source',
                  'course', 'deadline', 'period', 'published', 'tests']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

        teaching_courses = self.user.account.teaching_courses.all()
        assistant_courses = self.user.account.assistant_courses.all()
        self.fields['course'].queryset = (teaching_courses | assistant_courses).distinct()

        self.fields['title'].widget.attrs['class'] = 'form-control'
        self.fields['allowed_languages'].widget.attrs['class'] = 'w-100'
        self.fields['tags'].widget.attrs['class'] = 'w-100'
        self.fields['statement'].widget.attrs['class'] = 'form-control-file'
        self.fields['period'].widget.attrs['class'] = 'form-control'
        self.fields['source'].widget.attrs['class'] = 'form-control-file'
        self.fields['tests'].widget.attrs['class'] = 'form-control-file'

        self.fields['title'].widget.attrs['placeholder'] = 'Example Title'


class EditAssignmentForm(forms.ModelForm):
    class Meta:
        model = Assignment
        fields = ['title', 'allowed_languages', 'tags', 'statement',
                  'deadline', 'published', 'period', 'source', 'tests']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['title'].widget.attrs['class'] = 'form-control'
        self.fields['allowed_languages'].widget.attrs['class'] = 'w-100'
        self.fields['tags'].widget.attrs['class'] = 'w-100'
        self.fields['statement'].widget.attrs['class'] = 'form-control-file'
        self.fields['source'].widget.attrs['class'] = 'form-control-file'
        self.fields['deadline'] = forms.DateTimeField(
                                        required=False,
                                        widget=DateTimePicker(
                                            options={
                                                'useCurrent': True,
                                                'collapse': False,
                                            },
                                            attrs={
                                                'append': 'fa fa-calendar',
                                                'icon_toggle': True,
                                            },
                                        ),
                                    )
        self.fields['period'].widget.attrs['class'] = 'form-control'
        self.fields['tests'].widget.attrs['class'] = 'form-control-file'

        self.fields['title'].widget.attrs['placeholder'] = 'Example Title'


class CreateTagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'

$(document).ready(function () {

    $('.multiselect').select2();

    // add row on select
    let teacher_select = $('#id_teachers');
    teacher_select.on('select2:select', function (e) {
        let data = e.params.data;
        let id = data['id'];
        let text = data['text'].split(" ");
        let first_name = text[0];
        let last_name = text[1];
        let email = text[2].substring(1, text[2].length - 1);
        $('#teachers_table > tbody:last-child').append(
            '<tr id="teacher_deleteable_' + id + '">' +
            '<td>' + first_name + ' ' + last_name + '</td>' +
            '<td>' + email + '</td>' +
            '</tr>'
        );
    });

    // delete row on unselect
    teacher_select.on('select2:unselect', function (e) {
        let data = e.params.data;
        let id = data['id'];
        $('#teacher_deleteable_' + id).remove();
    });

    // add row on select
    let assistant_select = $('#id_assistants');
    assistant_select.on('select2:select', function (e) {
        let data = e.params.data;
        let id = data['id'];
        let text = data['text'].split(" ");
        let first_name = text[0];
        let last_name = text[1];
        let email = text[2].substring(1, text[2].length - 1);
        $('#assistants_table > tbody:last-child').append(
            '<tr id="assistant_deleteable_' + id + '">' +
            '<td>' + first_name + ' ' + last_name + '</td>' +
            '<td>' + email + '</td>' +
            '</tr>'
        );
    });

    // delete row on unselect
    assistant_select.on('select2:unselect', function (e) {
        let data = e.params.data;
        let id = data['id'];
        $('#assistant_deleteable_' + id).remove();
    });
});
from django.db import models
from django.urls import reverse
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User


class Course(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=25, unique=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.code + " : " + self.name

    def get_absolute_url(self):
        return reverse('course', kwargs={'pk': self.pk})


@receiver(post_save, sender=Course)
def create_course(sender, instance, created, **kwargs):
    """
    Add the current user as a teacher for this course

    :param sender: The model class. (Course)
    :param instance: The actual instance being saved.
    :param created: Boolean; True if a new record was created.
    """

    if created:
        user = instance.created_by
        user.account.teaching_courses.add(instance)

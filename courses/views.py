from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.db.models import Q
from django.core import serializers
import json
from courses.models import Course
from assignments.models import Assignment, Tag
from users.models import Account, User
from courses.forms import CourseForm, CourseEdit


class LandingPageView(TemplateView):
    template_name = "courses/landing_page.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = "Welcome"

        user = self.request.user

        if user.is_authenticated:

            # get courses as json
            teaching_courses = json.loads(serializers.serialize('json', user.account.teaching_courses.all()))
            assistant_courses = json.loads(serializers.serialize('json', user.account.assistant_courses.all()))

            # initialize courses
            courses = []

            # add courses where user is teacher
            for course in teaching_courses:
                pk = course['pk']
                code = course['fields']['code']
                name = course['fields']['name']
                courses.append({
                    'pk': pk,
                    'code': code,
                    'name': name,
                    'role': 'Teacher'
                })

            # add courses where user is assistant
            for course in assistant_courses:
                pk = course['pk']
                code = course['fields']['code']
                name = course['fields']['name']
                courses.append({
                    'pk': pk,
                    'code': code,
                    'name': name,
                    'role': 'Assistant'
                })

            context['courses'] = courses

        return context


class SearchCoursesView(TemplateView):
    template_name = "courses/search_courses.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Search Courses"
        courses = Course.objects.all()
        context['codes'] = courses.values_list('code', flat=True)
        context['names'] = courses.values_list('name', flat=True)
        return context


def courses_as_json(request):
    codes = request.GET.getlist('code[]')
    names = request.GET.getlist('name[]')

    q_objects = Q()  # Create an empty Q object to start with

    # 'or' the Q objects together
    if codes:
        for t in codes:
            q_objects |= Q(code=t)
    else:
        q_objects |= Q(code="")

    if names:
        for t in names:
            q_objects |= Q(name=t)
    else:
        q_objects |= Q(name="")

    courses = Course.objects.filter(q_objects)

    result_json = serializers.serialize('json', courses)
    return HttpResponse(result_json, content_type='application/json')


class SearchAssignmentsView(TemplateView):
    template_name = "courses/search_assignments.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Search Assignments"
        assignments = Assignment.objects.filter(published=True)
        tags = Tag.objects.all()
        context['titles'] = assignments.values_list('title', flat=True)
        context['tags'] = tags.values_list('name', flat=True)

        return context


def assignments_as_json(request):
    titles = request.GET.getlist('title[]')
    tags = request.GET.getlist('tag[]')
    q_objects = Q()  # Create an empty Q object to start with

    # 'or' the Q objects together
    if titles:
        for t in titles:
            q_objects |= Q(title=t)

    # 'or' the Q objects together
    if tags:
        tag_ids = []
        tag_objects = Tag.objects.filter(name__in=tags).all()
        for tag in tag_objects:
            tag_ids.append(tag.id)

        q_objects |= Q(tags__in=tag_ids)

    # get the assignments from the database
    assignments = Assignment.objects.filter(q_objects, published=True).distinct()

    # create a tag_id -> tag_name dictionary from the database
    tag_objects = Tag.objects.all()
    tag_dict = {}
    for object in tag_objects:
        tag_dict[object.id] = object.name

    # parses the assignments collection as a json string
    result_json_str = serializers.serialize('json', assignments)

    # transforms the string to a dictionary
    result_json = json.loads(result_json_str)

    # Changes the tags in the json from ids to names
    for model in result_json:
        model["fields"]["course"] = Course.objects.filter(id=model["fields"]["course"]).get().name
        model_tag_list = []
        for tag in model["fields"]["tags"]:
            model_tag_list.append(tag_dict[tag])

        model["fields"]["tags"] = model_tag_list

    # Passes the result dictionary to a json array
    result_json_str = json.dumps(result_json)

    return HttpResponse(result_json_str, content_type='application/json')


class CoursePageView(TemplateView):
    template_name = "courses/course_page.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        course = get_object_or_404(Course, id=self.kwargs['pk'])

        # Get teacher of this course
        teachers = Account.objects.filter(teaching_courses__id=course.id)

        # Get assistants of this course
        assistants = Account.objects.filter(assistant_courses__id=course.id)
        context['title'] = course.name
        context['course'] = course
        context['teachers'] = teachers
        context['assistants'] = assistants

        context['isTeacher'] = teachers.filter(id=self.request.user.id).exists()
        context['isAssistant'] = assistants.filter(id=self.request.user.id).exists()

        assignments = []
        if context['isTeacher'] or context['isAssistant']:
            assignments = Assignment.objects.filter(course=course)
        else:
            assignments = Assignment.objects.filter(course=course, published=True)

        periods = assignments.order_by('-deadline').values('period').all()

        assignment_groups = []
        period_dict = dict()
        for period in periods:
            period_val = period["period"]

            if not (period_val in period_dict):
                period_dict[period_val] = None
                element = dict()
                element["period"] = period_val
                element["assignments"] = assignments.filter(period=period_val)
                assignment_groups.append(element)

        context['assignment_groups'] = assignment_groups

        return context


class CreateCoursePageView(CreateView):
    template_name = "courses/course_form.html"
    model = Course
    form_class = CourseForm

    obj = ""

    def form_valid(self, form):
        # save user in created_by field
        self.obj = form.save(commit=False)
        self.obj.created_by = self.request.user

        # save object
        self.obj.save()

        # add teachers
        form.save_teachers(self.obj.id)

        # add assistants
        form.save_assistants(self.obj.id)

        return self.get_success_url()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Create Course"
        return context

    def get_form_kwargs(self, **kwargs):
        form_kwargs = super(CreateCoursePageView, self).get_form_kwargs()
        form_kwargs["user"] = self.request.user
        return form_kwargs

    def get_success_url(self):
        return HttpResponseRedirect(reverse('course', kwargs={'pk': self.obj.id}))


class EditCoursePageView(CreateView):
    template_name = "courses/course_edit.html"
    model = Course
    form_class = CourseEdit

    obj = ""

    def form_valid(self, form):
        self.obj = get_object_or_404(Course, id=self.kwargs['pk'])

        # update
        form.save_all(self.obj.id)

        return self.get_success_url()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = "Edit Course"

        course = get_object_or_404(Course, id=self.kwargs['pk'])

        # Get teacher of this course
        teachers = Account.objects.filter(teaching_courses__id=course.id)

        context['isTeacher'] = teachers.filter(id=self.request.user.id).exists()

        return context

    def get_form_kwargs(self, **kwargs):
        form_kwargs = super(EditCoursePageView, self).get_form_kwargs()
        form_kwargs["user"] = self.request.user
        form_kwargs['course'] = get_object_or_404(Course, id=self.kwargs['pk'])
        return form_kwargs

    def get_success_url(self):
        return HttpResponseRedirect(reverse('course', kwargs={'pk': self.obj.id}))

from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from courses.views import LandingPageView, SearchCoursesView, SearchAssignmentsView, CoursePageView, \
    CreateCoursePageView, courses_as_json, assignments_as_json, EditCoursePageView

urlpatterns = [
    path('', LandingPageView.as_view(), name='landing_page'),
    path('search_courses', SearchCoursesView.as_view(), name='search_courses'),
    path('search_assignments', SearchAssignmentsView.as_view(), name='search_assignments'),
    path('course/<int:pk>', CoursePageView.as_view(), name='course'),
    path('create_course', CreateCoursePageView.as_view(), name='create_course'),
    path('courses_as_json', courses_as_json, name='courses_as_json'),
    path('assignments_as_json', assignments_as_json, name='assignments_as_json'),
    path('edit_course/<int:pk>', EditCoursePageView.as_view(), name='edit_course')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

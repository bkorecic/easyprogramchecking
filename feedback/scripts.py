import os
from django.utils import timezone
from threading import Thread
from feedback.models import FeedbackGroup, Feedback, ErrorFeedback, PublicTestFeedback, SemiPrivateTestFeedback, \
    PrivateTestFeedback
from my_lib.zip_wrapper import extract_zip_file_to_folder, delete_folder_and_content

class SaveScriptProcess(Thread):

    def __init__(self, user, assignment, lang, client, zip_path, tests_path):
        Thread.__init__(self)
        self.user = user
        self.assignment = assignment
        self.lang = lang

        self.client = client
        self.zip_path = zip_path
        self.tests_path = tests_path

        # Save Feedback Group
        self.feedback_group = FeedbackGroup(user=self.user, assignment=self.assignment, done=False)
        self.feedback_group.save()

    def run(self):

        directory_path = self.zip_path.replace(".zip", "")

        # Unzip Zip
        try:
            extract_zip_file_to_folder(self.zip_path)
        except:
            #Delete Zip
            os.remove(self.zip_path)

            # Delete extracted zip
            delete_folder_and_content(directory_path)

            # Save Feedback Group with error
            self.feedback_group.error = "Invalid or corrupted zip file."
            self.feedback_group.done = True
            self.feedback_group.save()
            return

        # Delete zip file
        os.remove(self.zip_path)

        # Create array of scripts to test
        script_array = [directory_path + "/" + f for f in os.listdir(directory_path) if os.path.isfile(os.path.join(directory_path, f))]

        # Change permissions of script_array files to 750
        for script_path in script_array:
            os.chmod(script_path, 0o750)

        for script_path in script_array:
            # Change persmissions of script_path file to 755
            os.chmod(script_path, 0o755)
            submission = self.client.send_submission(script_path, self.tests_path, self.lang)
            file_name = script_path.split("/")[-1]
            if submission[0] == "success":
                self.save_feedback(submission[1], file_name)
            else:
                self.save_error_feedback(submission[1], file_name)

            # Change persmissions of script_path file to 750
            os.chmod(script_path, 0o750)

        self.feedback_group.done = True
        self.feedback_group.save()

        # Delete extracted zip
        delete_folder_and_content(directory_path)

        return

    def save_feedback(self, tests_arr, file_name):
        feedback_user = self.user
        feedback_assignment = self.assignment
        feedback_date = timezone.now()
        feedback = Feedback(user=feedback_user, assignment=feedback_assignment, date=feedback_date,
                            group=self.feedback_group, name=file_name)
        feedback.save()

        # Save the tests as single_test_feedback object of diferent types depending on the test type
        for test in tests_arr:
            single_test_feedback = None

            # Case the test is public
            if test[4] == "public":
                single_test_feedback = PublicTestFeedback(passed=bool(test[0]), input=test[1],
                                                          expected_output=test[2], actual_output=test[6], comment=test[3], error=test[5],
                                                          feedback=feedback)
            # Case the test is semi private
            elif test[4] == "semi_private":
                single_test_feedback = SemiPrivateTestFeedback(passed=bool(test[0]), input=test[1],
                                                               expected_output=test[2], actual_output=test[6], comment=test[3],
                                                               error=test[5],
                                                               feedback=feedback)
            # Case the test is private
            elif test[4] == "private":
                single_test_feedback = PrivateTestFeedback(passed=bool(test[0]), input=test[1],
                                                           expected_output=test[2], actual_output=test[6], comment=test[3], error=test[5],
                                                           feedback=feedback)
            single_test_feedback.save()

    def save_error_feedback(self, error_msg, file_name):
        feedback_user = self.user
        feedback_assignment = self.assignment
        feedback_date = timezone.now()
        feedback = ErrorFeedback(user=feedback_user, assignment=feedback_assignment, date=feedback_date,
                                 error=error_msg, group=self.feedback_group, name=file_name)
        feedback.save()

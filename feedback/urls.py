from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from feedback.views import ViewFeedback, feedback_group_as_csv,feedback_group_summary_as_csv

urlpatterns = [
    path('', ViewFeedback.as_view(), name='view_feedback'),
    path('feedback_group_as_csv', feedback_group_as_csv, name='feedback_group_as_csv'),
    path('feedback_group_summary_as_csv', feedback_group_summary_as_csv, name='feedback_group_summary_as_csv'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

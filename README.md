# EasyProgramChecking
<!-- TODO: Table of contents -->
## Resumen

Easy program checking es software orientado a la labor docente en el area de las ciencias de la computación.
Existen herramientas de evaluación automatizada que no necesitan intervención humana para entregar un resultado
y en el otro extremo se encuentra el no utilizar herramientas y hacer revisiones manuales. En medio de estas se encuentra
Easy Program Checking, un programa diseñado para dar feedback automatizado a los alumnos mientras realizan la tarea y una forma de evaluar
qué tipos de errores están cometiendo los estudiantes, de manera de facilitar la labor de corrección.

## Instalación

### Requerimientos:
* python 3
* pip 3
* Django==2.2.2
* django-tempus-dominus==5.1.2.5
* pytz==2019.1
* PyYAML==5.1
* pyyml==0.0.2
* sqlparse==0.3.0

La siguiente guía se hizo utilizando **Ubuntu 18.04.4 LTS**

### Setup del proyecto Django

1. Instalar pip: ```$ sudo apt update && sudo apt install python3-pip```


2. Crear un ambiente limpio para el proyecto (opcional, recomendado para desarrollo)
	1. Instalar virtualenv: ```$ sudo pip3 install virtualenv```
	2. Crear un ambiente virtual: ```$ virtualenv NAME -p python3```
	esto crea un ambiente virtual ubicado en el directorio actual. NAME es el nombre del entorno a crear
	3. Activar el ambiente virtual: ```$ source NAME/bin/activate``` **el tercer paso debe realizarse siempre que se quiera hacer deploy o desarrollar en el proyecto**
3. Instalar los requerimientos usados por el proyecto Django: ```$ pip3 install -r requirements.txt```

## Quickstart

### Primer despliegue
1. ejecutar el archivo corrector de tareas ```$ python3 run_server.py```
2. ejecutar el proyecto django ```$ python3 manage.py runserver```

Eso dejará easyprogramchecking en localhost:8000

### Setup de los usuarios
Inicialmente la base de datos no tiene usuarios, por lo que el usuario administrador se debe crear a mano ejecutando. ```$ python3 manage.py createsuperuser```

Una vez creado el usuario se debe abrir localhost:8000/admin e ingresar como administrador, ir a la pestaña usuarios y marcar el checkbox _can create_ para otorgar permisos de creación de cursos y tareas. **Este paso no funciona en firefox, en google chrome sí**

El login de easyprogramchecking solicita el nombre de usuario y password.
Para crear un usuario común se debe crear desde la pestaña _Sign up_ del login.

### Crear un curso
En un usuario con permisos _can create_ se pueden crear cursos en la pestaña create course.

### Crear una tarea
Logueado en un user con permisos _can create_ se pueden crear tareas para un curso en la pestaña del mismo con el boton create assignment.

Algunas cosas importantes:
    * La tarea estará disponible para los estudiantes y ayudantes si se marca el checkbox _is published_.
    * La opción _source_ esta disponible para subir el archivo .tex de la tarea.
    * La opción _period_ es para establecer el semestre de la tarea e.g. Otoño o Primavera.
    * Envíos masivos dejan un archivo csv en el directorio del proyecto. (bug-found)
    * Si se hace edit y submit de una tarea sin hacer cambios hay un error.(bug-found)

### Ejemplos de tareas input-output
En el directorio _examples_ se encuentran 2 tareas con ejemplos de test cases, formato de una entrega y formato para hacer corrección en batch.

* Watermelon:
  todo description

* Sudoku:
  todo description

## Casos de prueba
### Formato del archivo
El archivo debe contener un arreglo de diccionarios con 4 parametros:
- Input (String)
- Output (String)
- Comment (String) (se mostrara en el feedback del test)
- Test Type (String) de ["public", "semi_private", "private"] (private por defecto)

Los comentarios se mostraran en el feedback del test y los tipos de test tienen los siguientes significados.

- public: El feedback de este test sera un booleano indicando si el test paso, la salida esperada, la resultante y el comentario.
- semi_private: El feedback de este test sera un booleano indicando si el test paso y el comentario.
- private: Estos tests no son mostrados en el feedback.

Estas especificaciones son solo para los feedbacks recibidos por los estudiantes, los asistentes y profesores veran todos los tests como si fueran publicos.

### Ejemplos de archivo:

#### Json

```json
[
    {
        "Input": "[1,2,3,4]",
        "Output": "1",
        "Comment": "A Comment",
        "Test Type": "public"
    },
    {
        "Input": "[5,6,7,8]",
        "Output": "0",
        "Comment": "Another Coment",
        "Test Type": "private"
    }
]

```
#### Csv

Input,Output,Comment,Test Type
"[1,2,3,4]",1,A Comment,public
"[5,6,7,8]",0,Another Coment,private

#### Yml and Yaml

```yaml
- Input: "[1,2,3,4]"
  Output: '1'
  Comment: A Comment
  Test Type: public
- Input: "[5,6,7,8]"
  Output: '0'
  Comment: Another Coment
  Test Type: private
```

## Feature bag
* Agregar feedback de usuario cuando se hace un batch submit
* Cambiar la base de datos a postgres o alguna mas robusta que sqlite
* Dockerizar el proyecto

## To be fixed
<!-- TODO: Migrate "to be fixed" to GitLab issues -->

* Envíos masivos dejan un archivo csv en el directorio del proyecto. (bug-found)
* Si se hace edit y submit de una tarea sin hacer cambios hay un error.(bug-found)
* Migraciones en master no permiten hacer ``` $ python3 manage.py runserver``` responde: in parent Migration courses.0002_auto_20190620_1445 dependencies reference nonexistent parent node ('courses', '0001_initial')

    quickfix
```$ rm -rf courses/migrations && python3 manage.py makemigrations && python3 manage.py migrate --run-syncdb``` y luego ``` $ python3 manage.py runserver```

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from courses.models import Course


def get_name_and_email(self):
    return self.first_name + " " + self.last_name + " (" + self.email + ")"


User.add_to_class("__str__", get_name_and_email)


class Account(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    can_create = models.BooleanField(default=False)
    teaching_courses = models.ManyToManyField(Course, related_name="teaching_courses", blank=True)
    assistant_courses = models.ManyToManyField(Course, related_name="assistant_courses", blank=True)

    def __str__(self):
        return self.user.email


@receiver(post_save, sender=User)
def create_user_account(sender, instance, created, **kwargs):
    """
    Create account object associated to user

    :param sender: The model class. (User)
    :param instance: The actual instance being saved.
    :param created: Boolean; True if a new record was created.
    """

    if created:
        Account.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_account(sender, instance, **kwargs):
    """
    Update associated account on user update

    :param sender: The model class. (User)
    :param instance: The actual instance being updated.
    """

    instance.account.save()

# Cuarto reporte
* Se trabaja en el tercer milestone

## Milestone 3: Crear lista de ejemplos de tareas con multiples soluciones correctas y scripts python que verifican respuestas clasifican las respuestas. Ejemplos:
	- Partial order de cursos en curriculum (Topo sort).
	- Posicion de un elemento en arreglo desordenado.

- investigar como se protege epc del codigo de los alumnos  
- numeros negativos representan errores y positivos tipos de acierto  
- recibir input desde std input  
- idea proveer un parse_input en un utils.py (porque se va a repetir en todos los programas)

### Vulnerabilidad
EasyProgramChecking no se protege de ninguna forma de programas maliciosos

### Partial order de cursos

El input consiste en una serie de lineas donde el primer numero corresponde a un nodo y los siguientes corresponden a los requisitos para tomar ese curso

e.g

```
input valido

7
1
2
3 1 2
4 3
5 4
6 3
7 5 6

output valido

1
2
3
4
6
5
7
```

```python
def inputToGraph(intput):
    adj= {}
    lines = input.split("\n")
    for line in lines:
        line = line.split(" ")
        for i in range(len(line)):
            line[i] = int(line[i])

        node = line[0]
        restrictions = line[1:]

        adf[node] = restrictions

    return adj

def removeRestriction(adj,restriction):
    for node in adj.keys():
        adj[node] = adj[node].remove(restriction)
        
    return adj
    

def parse_input(input):
    i = input.split("$$")[0]
    o = input.split("$$")[1]
    return (i,o)


def check(input, output):
    adj = inputToGraph(input)
    lines = output.split("\n")
    for node in lines:
        if adj[node]:
            return -1
        else:
            adj = removeRestriction(node)

    return 1

def run():
    _input = input()
    (i,o) = parse_input(_input)
    return check(i,o)

run()

``` 
En este caso tenemos 2 grupos [-1,1].
- -1 Representa quienes toman un curso para el que no cumplen los requisitos
- 1 Representa quienes toman los cursos en un orden correcto

### Posicion de un elemento en el arreglo
El input consiste en una lista de numeros separados por espacios, luego una coma y el numero que se quiere buscar

```
input valido

1 3 5 7 1 3, 1

output valido
4
```

```python
def parse_input(input):
    i = input.split("$$")[0]
    o = input.split("$$")[1]
    return (i,o)

def proccess_input(input):
    pair = input.split(", ")
    input = [int(i) for i in input.split(" ")]
    query = int(pair[1])
    return (input,query)

def check(input, output):
    (input,query) = proccessInput(input)

""" query is not in the input """
    if(output==-1):
        if(elem in input):
            return 0 
        else:
            return 1

""" query is in the input """
    if(input[output] == query):
        return 2
    else:
        return 3

def run():
    _input = input()
    (i,o) = parse_input(_input)
    return check(i,o)

run()


```
En este caso tenemos 4 grupos [-2,-1,1,2]. 
- -1 Representa a quienes responden que el elemento no esta cuando el elemento si estaba en el arreglo. 
- -2 Representa a quienes responden una posicion equivocada para una query
- 1 Representa a los que reconocen que un elemento efectivamente no esta en el arreglo
- 2 Representa a lo que encuentran correctamente la query en el arreglo


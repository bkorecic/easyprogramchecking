# Primer reporte
* Se trabaja en los milestones 1 y 2

## Milestone 1: Crear nuevo branch gitlab para agregar nuevas funcionalidades
* Se crea nueva rama 'trabajoDirigido'
* Se establecen las condiciones del trabajo dirigido conversadas en la reunión del día 15/04/2020
* Se crea nueva rama 'semana1' para los cambios del primer reporte 

Milestone 1 completado

## Milestone 2: Draft de manual de instalacion de easy program checking
* Reunión con Sven el día 16/04/2020: Se resuelven dudas sobre la instalación y preparación para easy program checking
* Primera parte del manual de instalación

* TODOS
    1. [ ] Ver de nuevo el video de la reunión para completar el manual de instalación
    2. [ ] Incorporar al manual la creación de un usuario administrador
    3. [ ] Incorporar al manual la creacion de el resto de los tipos de usuarios
    3. [ ] Incorporar al manual la secuencia de acciones para crear una tarea
    4. [ ] Incorporar al manual la secuencia de acciones para definir tests
    5. [ ] Incorporar ejemplos de tareas input output
    6. [ ] Incorporar ejemplos de tests para tareas input output
    3. [ ] Incorporar al manual la secuencia de acciones para hacer upload de una tarea
    3. [ ] Incorporar al manual la secuencia de acciones para hacer un batch upload de tareas
    4. [ ] Incorporar al manual la secuencia de acciones para hacer download de tareas
    7. [ ] Averiguar si se puede hacer batch download
    

## Manual de instalación

### Requerimientos:
* python 3
* pip 3
* Django==2.2.2
* django-tempus-dominus==5.1.2.5
* pytz==2019.1
* PyYAML==5.1
* pyyml==0.0.2
* sqlparse==0.3.0

La siguiente guia se hizo utilizando **Ubuntu 18.04.4 LTS**

1. Instalar pip: ```sudo apt update && sudo apt install python3-pip```

2. Crear un ambiente limpio para el proyecto (opcional, recomendado para desarrollo)
	1. Instalar virtualenv: ```sudo pip3 install virtualenv```
	2. Crear un ambiente virtual: ```virtualenv NAME -p python3``` 
	esto crea un ambiente virtual ubicado en el directorio actual. NAME es el nombre del entorno a crear
	3. Activar el ambiente virtual: ```source NAME/bin/activate``` **el tercer paso debe realizarse siempre que se quiera hacer deploy o desarrollar en el proyecto**
3. Instalar los requerimientos usados por el proyecto django: ```pip3 install -r requirements.txt```
 


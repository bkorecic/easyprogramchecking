import os.path
from subprocess import Popen, PIPE, TimeoutExpired, call

script_dict = {}


class Script:

    def __init__(self, path):
        self.path = path

    def get_process(self):
        """
        Creates a new subprocess to run the script and returns it
        :return: Subprocess
        """
        pass

    def run(self, test_input, timeout):
        """
        Runs a script with some input and a timeout in seconds and returns what's in stdout, an error code and if it was
        killed because of the timeout
        :param test_input: str
        :param timeout: int
        :return: str, str, bool
        """
        p = self.get_process()
        if test_input != '':
            p.stdin.write(test_input)

        killed_by_time = False
        try:
            out, err = p.communicate(timeout=timeout)
        except TimeoutExpired:
            print('TimeOutExpired')
            p.kill()
            out, err = p.communicate()
            killed_by_time = True

        return out, err, killed_by_time

    def process_error(self, err):
        # No Error
        if len(err) == 0:
            return 0
        # Error
        else:
            return 1


script_dict["Python 3"] = lambda path: (Python3Script(path))
class Python3Script(Script):

    def __init__(self, path):
        Script.__init__(self, path)

    def get_process(self):
        return Popen(['python3', self.path], stdin=PIPE, stdout=PIPE, stderr=PIPE)


script_dict["Python 2"] = lambda path: (Python2Script(path))
class Python2Script(Script):

    def __init__(self, path):
        Script.__init__(self, path)

    def get_process(self):
        return Popen(['python', self.path], stdin=PIPE, stdout=PIPE, stderr=PIPE)


script_dict["C++11"] = lambda path: (Cpp11Script(path))
class Cpp11Script(Script):

    def __init__(self, path):
        Script.__init__(self, path)
        self.compiled_path = os.path.splitext(self.path)[0] + '.out'
        call(['g++', '-std=c++11', '-o', self.compiled_path, self.path])

    def get_process(self):
        return Popen([self.compiled_path], stdin=PIPE, stdout=PIPE, stderr=PIPE)


script_dict["Java 11"] = lambda path: (Java11Script(path))
class Java11Script(Script):

    def __init__(self, path):
        Script.__init__(self, path)
        self.is_compiled = True
        call(['javac', self.path])

    def get_process(self):
        compiled_path = os.path.splitext(self.path)[0]
        return Popen(['java', '-cp', os.path.dirname(compiled_path), os.path.basename(compiled_path)], stdin=PIPE, stdout=PIPE, stderr=PIPE)
